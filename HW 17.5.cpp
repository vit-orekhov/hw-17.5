
#include <iostream>
#include <cstdio>
#include <math.h>
#include<Windows.h>

class OwnVector 
{
private:

    double X;
    double Y;
    double Z;
   
public:



    double GetX()
    {
        return X;
    }
    double GetY()
    {
        return Y;
    }
    double GetZ()
    {
        return Z;
    }
    



    void SetX(double X)
    {
        this->X = X;
    }

    void SetY(double Y)
    {
        this->Y = Y;
    }

    void SetZ(double Z)
    {
        this->Z = Z;
    }

  


    void Show()
    {
        double X1;
        double Y1;
        double Z1;
          
        std::cout << "\nCoordinates: " << GetX() << " " << GetY() << " " << GetZ();
             
      
    }

    double Scalar()
    {
        
        double Scalar1;
        
        Scalar1 = 0;              
        Scalar1 = (sqrt( (GetX()*GetX()) + (GetY() * GetY()) + (GetZ() * GetZ())));
        
       

        std::cout << "\nScalar (Length) of your vector defined by coordinates: " << Scalar1;

        return Scalar1;
    }

  

};

int main()
{


    double X1;
    double Y1;
    double Z1;
    std::cout << "Now we calculate Length of your vector\nAt first put in wished coordinates";
    std::cout << "\nEnter X-point: ";
    std::cin >> X1;
    std::cout << "Enter Y-point: ";
    std::cin >> Y1;
    std::cout << "Enter Z-point: ";
    std::cin >> Z1;
    
    OwnVector V;
    V.SetX(X1);
    V.SetY(Y1);
    V.SetZ(Z1);
    V.Show();

    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, FOREGROUND_RED);

    V.Scalar();

    HANDLE handle2 = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle2, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);


    return 0;
}